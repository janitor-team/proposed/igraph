# /usr/share/doc/libigraph-dev/examples/simple/Makefile
#
# Ad hoc Makefile for building and playing with the sample sources
# distributed within the debian package libigraph-examples.
#
# Recommended usage:
#  create a dedicated folder somewhere in your HOME directory;
#  link all the files in /usr/share/doc/libigraph-dev/examples/simple
#  in the dedicated folder; launch this Makefile in the dedicated folder:
#  $ make
#  for a basic cleanup, consider the clean target:
#  $ make clean
#  for an entire cleanup, the maintainer-clean target:
#  $ make maintainer-clean
#  for other targets, just read the Makefile.
#
# written for Debian by Jerome Benoit <calculus@rezozer.net>
# on behalf of the Debian Med Packaging Team
# copyright: 2020-2022 Jerome Benoit <calculus@rezozer.net>
# distributed under the terms and conditions of GPL version 2 or later
#

SHELL=/bin/bash

default: all

XPROGRAMS = \
	tls1

XOUTPOUTS = \
	igraph_power_law_fit.out

PROGRAMS = \
	$(patsubst %.c,%,$(wildcard *.c))

RESULTFILES = \
	$(addsuffix .res,$(filter-out $(XPROGRAMS),$(PROGRAMS)))

DIFFFILES = \
	$(patsubst %.out,%.diff,$(filter-out $(XOUTPOUTS),$(wildcard *.out)))

XDIFFFILES = \
	$(patsubst %.out,%.xdiff,$(filter $(XOUTPOUTS),$(wildcard *.out)))

CFLAGS = $(shell pkg-config igraph --cflags)
LDLIBS = $(shell pkg-config igraph --libs) -lm

CFLAGS += -Wall -g
CFLAGS += -Wno-maybe-uninitialized

all: build check

build: $(PROGRAMS)

checkclean:
	$(RM) $(XDIFFFILES) $(DIFFFILES) $(RESULTFILES)

check-run: $(RESULTFILES)

check-res: $(DIFFFILES) $(XDIFFFILES)

check: checkclean check-run check-res

clean: checkclean
	$(RM) $(PROGRAMS)

maintainer-clean: clean

%.res %.xres : %
	@echo "===8><--- $* ---"
	( set -o pipefail ; ./$< | tee $@ )
	@echo "----------><8==="
	@echo

%.diff : %.res %.out
	@echo "===8><--- $* ---"
	( set -o pipefail ; diff -N $^ | tee $@ )
	@echo "----------><8==="
	@echo

%.xdiff : %.res %.out
	@echo "===8><--- $* ---"
	diff -N $^ | tee $@
	@echo "----------><8==="
	@echo

%.out : %
#### do nothing

.PRECIOUS: %.res %.xres

.NOTPARALLEL:
